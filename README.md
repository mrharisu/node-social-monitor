# Project Title

Initial start of social media monitoring tool.

## Getting Started

NPM install
Can also install forever globally and run bot with forever

### Prerequisites

NODE

### Installing

NPM install

## Running the tests

No tests setup yet

## Contributing


## Versioning


## Authors

* **Jon Stephenson** - *Initial work*

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
