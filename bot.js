var Twitter = require('twitter');
var config = require('./config/config');
var client = new Twitter(config);
var query = 'list:keepdonhonest/keepingtrumphonest';

console.log('|| started Date - ' + new Date() + ' ||');

var repeatedTweetId = 0;
function searchAndTweet(succeed, fail) {
  client.get('search/tweets', {q: query, result_type: 'recent', count: 1}, function(err, tweets, response) {
    if (!tweets.statuses) {
      fail(err);
    }
    tweets.statuses.forEach(function(tweet) {
      var tweetId = tweet.id_str;
      if (repeatedTweetId != tweetId) {
        console.log('Retweeting id:' + tweetId);
        client.post('statuses/retweet/' + tweetId, function(err, tweet, id) {
          console.log(err || tweet.text);
        });
      } else {
        console.log ('repeated tweet id:' + repeatedTweetId);
      }
      repeatedTweetId = tweet.id_str;
    });
  });
}

setInterval(function() {
  searchAndTweet(console.log, console.log);
}, 12 * 1000);
